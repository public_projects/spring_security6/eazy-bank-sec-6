create database easybank;

use eazybank;

create table users
(
    username varchar(50) not null,
    password varchar(500) not null,
    enabled int not null,
    primary key (username)
);

create table authorities
(
    username varchar(50) not null,
    authority varchar(50) not null,
    constraint fk_authorities_users foreign key (username) references users (username)
);

create unique index ix_auth_username on authorities (username, authority);

INSERT INTO users (username, password, enabled)
VALUES ('happy', '123456', 1);

INSERT INTO authorities (username, authority)
VALUES ('happy', 'write');


create table customer
(
    id int not null auto_increment,
    email varchar(45) not null,
    pwd varchar(200) not null,
    role varchar(45) not null,
    primary key (id)
);

INSERT INTO customer (email, pwd, role)
VALUES
    ('happy', '123', 'admin');